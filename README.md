# Projet RDF

<div align="center">
<img width="400" height="300" src="jena.png">
</div>

## Description du projet

Bibliothèque Jena réalisée avec Java SE en MASTER INFO 1 à l'université d'Angers dans le cadre du module "Web des données" durant l'année 2019-2020. \
Ce projet encapsule la bibliothèque Jena afin de "faciliter" et "simplifier" des traitements en accès, lecture et écriture
sur les modèles RDF dans n'importe quelle langage (N3, RDF-XML, etc.).

## Acteurs

### Réalisateurs

Ce projet a été réalisé par :
- Théo MAHAUDA : tmahauda@etud.univ-angers.fr.

### Encadrants

Ce projet fut encadré par un enseignant de l'unversité d'Angers :
- David GENEST : david.genest@univ-angers.fr.

## Organisation

Ce projet a été agit au sein de l'université d'Angers dans le cadre du module "Web des données" du MASTER INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2020 sur la période du module pendant les heures de TP et personnelles à la maison.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Jena ;
- RDF ;
- RDF-Schéma ;
- OWL ;
- SPARQL.