package universite.angers.master.info.rdf;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;
import org.apache.log4j.Logger;
import universite.angers.master.info.rdf.graph.GraphAdapter;
import universite.angers.master.info.rdf.graph.GraphFactory;

/**
 * Test sur les données de la société de productions à travers des exercices
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class TestAbonnement {

	private static final Logger LOG = Logger.getLogger(TestAbonnement.class);

	/**
	 * Propriétés schéma
	 */
	private static final String URI_ABONNEMENT_SCHEMA = "http://www.m1wd.org/abonnement";
	private static final String PATH_ABONNEMENT_SCHEMA = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/abonnements/Abonnements_Schema.n3";

	/**
	 * Propriétés data
	 */
	private static final String URI_ABONNEMENT_DATA = "http://www.m1wd.org/abonnement";
	private static final String PATH_ABONNEMENT_DATA = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/abonnements/Abonnements_Data.n3";

	/**
	 * Propriétés data schema
	 */
	private static final String URI_ABONNEMENT_DATA_SCHEMA = "http://www.m1wd.org/abonnement";
	private static final String PATH_ABONNEMENT_DATA_SCHEMA = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/abonnements/Abonnements_Data_Schema.n3";
	
	/**
	 * Le graph qui contient les modèles des abonnements
	 */
	private static GraphAdapter<OntModel, OntModel, InfModel> graphAbonnement;
	
	/**
	 * Executer le test sur les modèles des abonnements
	 * @param args
	 */
	public static void main(String[] args) {
		load();
		displayClients();
		displayAllAboData();
		buildSchemaModelFromJena();
	}

	/**
	 * Charge, affiche et valide le graphe
	 */
	private static void load() {
		graphAbonnement = GraphFactory.buildGraphOntModelFromFactory(
				PATH_ABONNEMENT_SCHEMA, Lang.N3, URI_ABONNEMENT_SCHEMA, 
				PATH_ABONNEMENT_DATA, Lang.N3, URI_ABONNEMENT_DATA, 
				PATH_ABONNEMENT_DATA_SCHEMA, Lang.N3, URI_ABONNEMENT_DATA_SCHEMA);
		
		graphAbonnement.read();
		graphAbonnement.print();
		graphAbonnement.valid();
	}
	
	/**
	 * Construit un RDF SCHEMA à partir de Jena
	 */
	private static void buildSchemaModelFromJena() {
		OntModel schema = ModelFactory.createOntologyModel(OntModelSpec.RDFS_MEM_TRANS_INF);
		schema.setNsPrefix("abo", URI_ABONNEMENT_SCHEMA+"#");

		OntClass abo = schema.createClass(URI_ABONNEMENT_SCHEMA+"#Abonnement");
		
		//ABONNEMENT
		
		OntClass aboData = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementData");
		aboData.addSuperClass(abo);
		
		OntClass aboSMS = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementSMS");
		aboSMS.addSuperClass(abo);
		
		OntClass aboPhone = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementPhone");
		aboPhone.addSuperClass(abo);
		
		//FIXE
		
		OntClass aboFixe = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementFixe");
		aboFixe.addSuperClass(aboData);
		
		OntClass aboFixeFibre = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementFixeFibre");
		aboFixeFibre.addSuperClass(aboFixe);
		
		OntClass aboFixeADSL = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementFixeADSL");
		aboFixeADSL.addSuperClass(aboFixe);
		
		//FIXE PHONE
		
		OntClass aboFixePhone = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementFixePhone");
		aboFixePhone.addSuperClass(aboFixe);
		aboFixePhone.addSuperClass(aboPhone);
		
		OntClass aboFixePhoneFibre = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementFixePhoneFibre");
		aboFixePhoneFibre.addSuperClass(aboFixeFibre);
		aboFixePhoneFibre.addSuperClass(aboPhone);
		
		OntClass aboFixePhoneADSL = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementFixePhoneADSL");
		aboFixePhoneADSL.addSuperClass(aboFixeADSL);
		aboFixePhoneADSL.addSuperClass(aboPhone);

		//FIXE SMS
		
		OntClass aboFixeSMS = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementFixeSMS");
		aboFixeSMS.addSuperClass(aboFixe);
		aboFixeSMS.addSuperClass(aboSMS);
		
		OntClass aboFixeSMSFibre = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementFixeSMSFibre");
		aboFixeSMSFibre.addSuperClass(aboFixeFibre);
		aboFixeSMSFibre.addSuperClass(aboSMS);
		
		OntClass aboFixeSMSADSL = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementFixeSMSADSL");
		aboFixeSMSADSL.addSuperClass(aboFixeADSL);
		aboFixeSMSADSL.addSuperClass(aboSMS);
		
		//MOBILE
		
		OntClass aboMobile = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementMobile");
		aboMobile.addSuperClass(aboSMS);
		
		OntClass aboMobilePhone = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementMobilePhone");
		aboMobilePhone.addSuperClass(aboMobile);
		aboMobilePhone.addSuperClass(aboPhone);
		
		OntClass aboMobileData = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementMobileData");
		aboMobileData.addSuperClass(aboMobile);
		aboMobileData.addSuperClass(aboData);
		
		OntClass aboMobilePhoneData = schema.createClass(URI_ABONNEMENT_SCHEMA+"#AbonnementMobilePhoneData");
		aboMobilePhoneData.addSuperClass(aboMobile);
		aboMobilePhoneData.addSuperClass(aboPhone);
		aboMobilePhoneData.addSuperClass(aboData);
		
		//Attention ! Il faut bien utiliser createOntProperty et non createObjectProperty qui est reservé à OWL
		OntProperty aboNom = schema.createOntProperty(URI_ABONNEMENT_SCHEMA+"#aboNom");
		aboNom.addDomain(abo);
		aboNom.addRange(XSD.xstring);
		
		OntProperty aboTarif = schema.createOntProperty(URI_ABONNEMENT_SCHEMA+"#aboTarif");
		aboTarif.addDomain(abo);
		aboTarif.addRange(XSD.integer);
		
		OntProperty aboDataProp = schema.createOntProperty(URI_ABONNEMENT_SCHEMA+"#aboData");
		aboDataProp.addDomain(abo);
		aboDataProp.addRange(XSD.integer);
		
		OntProperty aboDataTelephone = schema.createOntProperty(URI_ABONNEMENT_SCHEMA+"#aboDataTelephone");
		aboDataTelephone.addDomain(aboPhone);
		aboDataTelephone.addRange(XSD.integer);
		aboDataTelephone.addSuperProperty(aboDataProp);
		
		OntProperty aboDataInternet = schema.createOntProperty(URI_ABONNEMENT_SCHEMA+"#aboDataInternet");
		aboDataInternet.addDomain(aboData);
		aboDataInternet.addRange(XSD.integer);
		aboDataInternet.addSuperProperty(aboDataProp);
		
		OntClass client = schema.createClass(URI_ABONNEMENT_SCHEMA+"#Client");
		OntClass souscription = schema.createClass(URI_ABONNEMENT_SCHEMA+"#Souscription");
		
		OntProperty clientNom = schema.createOntProperty(URI_ABONNEMENT_SCHEMA+"#clientNom");
		clientNom.addDomain(client);
		clientNom.addRange(XSD.xstring);
		
		OntProperty clientNumero = schema.createOntProperty(URI_ABONNEMENT_SCHEMA+"#clientNumero");
		clientNumero.addDomain(client);
		clientNumero.addRange(XSD.integer);
		
		OntProperty clientSousc = schema.createOntProperty(URI_ABONNEMENT_SCHEMA+"#clientSousc");
		clientSousc.addDomain(client);
		clientSousc.addRange(souscription);
		
		OntProperty souscPossedeAbo = schema.createOntProperty(URI_ABONNEMENT_SCHEMA+"#souscPossedeAbo");
		souscPossedeAbo.addDomain(souscription);
		souscPossedeAbo.addRange(abo);
		
		OntProperty souscPossedeNumero = schema.createOntProperty(URI_ABONNEMENT_SCHEMA+"#souscPossedeNumero");
		souscPossedeNumero.addDomain(souscription);
		souscPossedeNumero.addRange(XSD.integer);

		schema.write(System.out, Lang.N3.getName());
	}
	
	/**
	 * Écrire un programme qui affiche tous les numéros de clients, et pour chaque client affiche les noms
	 * des abonnements de ce client et le nombre d’abonnements du client
	 */
	private static void displayClients() {
		Resource client = graphAbonnement.getDataSchema().getSubject("Client");
		LOG.debug("Resource client : " + client);
		
		Resource souscription = graphAbonnement.getDataSchema().getSubject("Souscription");
		LOG.debug("Resource souscription : " + souscription);
		
		Property numeroClient = graphAbonnement.getDataSchema().getPredicat("clientNumero");
		LOG.debug("Numéro client : " + numeroClient);
		
		Property souscClient = graphAbonnement.getDataSchema().getPredicat("clientSousc");
		LOG.debug("Soucription client : " + souscClient);
		
		Property aboClient = graphAbonnement.getDataSchema().getPredicat("souscPossedeAbo");
		LOG.debug("Abonnement client : " + aboClient);
		
		StmtIterator iterClient = graphAbonnement.getDataSchema().getModel().listStatements(null, RDF.type, client);
		
		while(iterClient.hasNext()) {
			Statement stmtClient = iterClient.nextStatement();
			Resource subjectClient = stmtClient.getSubject();
			
			LOG.debug("Stmt client : " + stmtClient);
			LOG.debug("Client : " + subjectClient);
			
			//On récupère le numéro client
			Statement stmtNumClient = subjectClient.getProperty(numeroClient);
			int numClient = stmtNumClient.getObject().asLiteral().getInt();
			
			LOG.debug("Numéro client : " + numClient);
			
			//Pour chaque client on affiche ses abonnements qu'il possède
			StmtIterator iterSousc = subjectClient.listProperties(souscClient);
			
			LOG.debug("**Client (" + numClient + ") possède les abonnements suivants : ");
			
			while(iterSousc.hasNext()) {
				Statement stmtSousc = iterSousc.nextStatement();
				Resource subjectAbo = stmtSousc.getObject().asResource();

				StmtIterator iterAbo = subjectAbo.listProperties(aboClient);
				
				while(iterAbo.hasNext()) {
					Statement stmtAbo = iterAbo.nextStatement();
					LOG.debug("**** : " + stmtAbo.getObject());
				}
			}
		}
	}
	
	/**
	 * Écrire un programme qui affiche tous les abonnements données. Expliquer brièvement les résultats
	 * produits. Si les résultats produits ne sont pas les résultats attendus, expliquer pourquoi.
	 */
	private static void displayAllAboData() {
		Resource aboData = graphAbonnement.getDataSchema().getSubject("AbonnementData");
		LOG.debug("Resource abo data : " + aboData);
		
		StmtIterator iterAboData = graphAbonnement.getDataSchema().getModel().listStatements(null, RDF.type, aboData);
		
		while(iterAboData.hasNext()) {
			Statement stmtAboData = iterAboData.nextStatement();
			
			LOG.debug("Abonnement qui ont de la donnée : " + stmtAboData.getSubject());
		}
	}
}
