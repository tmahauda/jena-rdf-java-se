package universite.angers.master.info.rdf;

import org.apache.jena.rdf.model.*;
import org.apache.jena.util.*;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.reasoner.*;
import java.util.*;

public class TestOwl {
	
	private static final String PATH_SCHEMA = "file:///media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/EvalOWL/src/main/resources/rdf/ordinateurs/Ordinateurs_Schema.owl";
	private static final String PATH_DATA = "file:///media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/EvalOWL/src/main/resources/rdf/ordinateurs/Ordinateurs_Data.rdf";
	private static InfModel infModel;
	
	private static void printStatements(Model m, Resource s, Property p, Resource o) {
		for (StmtIterator i = m.listStatements(s, p, o); i.hasNext();) {
			Statement stmt = i.nextStatement();
			System.out.println(" - " + PrintUtil.print(stmt));
		}
	}
	
	private static void load() {
		// Chargement des documents.
		Model schema = FileManager.get().loadModel(PATH_SCHEMA);
		Model data = FileManager.get().loadModel(PATH_DATA);
				
		// Initialisation du reasoner.
		Reasoner reasoner = ReasonerRegistry.getOWLReasoner();
		reasoner = reasoner.bindSchema(schema);
		infModel = ModelFactory.createInfModel(reasoner, data);
	}
	
	private static void displayAllTriplets(String subject) {
		Resource resource = infModel.getResource(subject);
		System.out.println("All triplets : ");
		printStatements(infModel, resource, null, null);
	}
	
	private static void displayAllClass(String instance) {
		Resource resource = infModel.getResource(instance);
		System.out.println("All class : ");
		printStatements(infModel, resource, RDF.type, null);
	}
	
	private static void displayAllSubject(String type) {
		Resource resource = infModel.getResource(type);
		System.out.println("All Subject : ");
		printStatements(infModel, null, RDF.type, resource);
	}
	
	private static void validate() {
		System.out.println("Valid : ");
		ValidityReport validity = infModel.validate();
		if (validity.isValid())
			System.out.println("OK");
		else {
			System.out.println("Erreurs");
			for (Iterator<ValidityReport.Report> i = validity.getReports(); i.hasNext();) {
				ValidityReport.Report report = i.next();
				System.out.println(" - " + report);
			}
		}
	}
	
	public static void main(String args[]) {
		load();
		validate();
		displayAllClass("http://www.exemple.com#MBP");
		displayAllTriplets("http://www.exemple.com#MBP");
		displayAllSubject("http://www.exemple.com#MBP");
	}
}
