package universite.angers.master.info.rdf;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.RDF;
import org.apache.log4j.Logger;
import universite.angers.master.info.rdf.graph.GraphAdapter;
import universite.angers.master.info.rdf.graph.GraphFactory;

/**
 * Test sur les données des albums photos à travers des exercices
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class TestPhotos {

	private static final Logger LOG = Logger.getLogger(TestPhotos.class);

	/**
	 * Propriétés schéma
	 */
	private static final String URI_PHOTO_SCHEMA = "http://www.m1wd.org/album/photo";
	private static final String PATH_PHOTO_SCHEMA = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/photos/Photos_Schema.n3";
	
	/**
	 * Propriétés data
	 */
	private static final String URI_PHOTO_DATA = "http://www.m1wd.org/album/photo";
	private static final String PATH_PHOTO_DATA = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/photos/Photos_Data.n3";

	/**
	 * Propriétés data schema
	 */
	private static final String URI_PHOTO_DATA_SCHEMA = "http://www.m1wd.org/album/photo";
	private static final String PATH_PHOTO_DATA_SCHEMA = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/photos/Photos_Data_Schema.n3";
	
	/**
	 * Le graphe qui contient les 3 modèles
	 */
	private static GraphAdapter<OntModel, OntModel, InfModel> graphPhoto;
	
	public static void main(String[] args) {
		load();
		checkGraph();
		displayAlbum();
		displayPortraits();
	}

	/**
	 * Charge, affiche et valide le graphe
	 */
	private static void load() {
		graphPhoto = GraphFactory.buildGraphOntModelFromFactory(
				PATH_PHOTO_SCHEMA, Lang.N3, URI_PHOTO_SCHEMA, 
				PATH_PHOTO_DATA, Lang.N3, URI_PHOTO_DATA, 
				PATH_PHOTO_DATA_SCHEMA, Lang.N3, URI_PHOTO_DATA_SCHEMA);
		
		graphPhoto.read();
		graphPhoto.print();
		graphPhoto.valid();
	}
	
	/**
	 * Utiliser ce programme pour vérifier les fichiers écrits dans la feuille de TP
	 * 1. Remarquer l’usage du prédicat a qui peut être utilisé en Notation 3 à la
	 * place de rdf:type
	 */
	private static void checkGraph() {
		boolean valid = graphPhoto.valid();
		LOG.debug("Graph is valid : " + valid);
	}
	
	/**
	 * Écrire un programme qui affiche toutes les albums (l’URI de l’album, inutile d’afficher le nom) et pour
	 * chaque album, affiche les URI des portraits de cet album
	 */
	private static void displayAlbum() {
		Resource album = graphPhoto.getDataSchema().getSubject("Album");
		LOG.debug("Resource album : " + album);
		
		Resource portrait = graphPhoto.getDataSchema().getSubject("Portrait");
		LOG.debug("Resource portrait : " + portrait);
		
		Property contient = graphPhoto.getDataSchema().getPredicat("contient");
		LOG.debug("Predicat contient : " + contient);
		
		//Itérateur album
		StmtIterator iterAlbum = graphPhoto.getDataSchema().getModel().listStatements(null, RDF.type, album);
		
		//Parcours de chaque album
		while(iterAlbum.hasNext()) {
			Statement stmtAlbum = iterAlbum.nextStatement();
			Resource subjectAlbum = stmtAlbum.getSubject();

			LOG.debug("**album : " + subjectAlbum + " contient les portraits suivants : ");
			
			//Pour chaque album on affiche les photos qu'il contient
			StmtIterator iterAlbumPhoto = subjectAlbum.listProperties(contient);
			
			//Parcours toutes les photos de l'album
			while(iterAlbumPhoto.hasNext()) {
				Statement stmtPhoto = iterAlbumPhoto.nextStatement();
				
				RDFNode objectPhoto = stmtPhoto.getObject();
				//LOG.debug("Object photo : " + objectPhoto);

				Resource photo = objectPhoto.asResource();
				//LOG.debug("Resource photo : " + photo);
				
				boolean isPortrait = graphPhoto.getDataSchema().resourceContainsType(photo, portrait);

				if(isPortrait) {
					LOG.debug("****photo " + photo);
				}
			}
		}
	}
	
	/**
	 * Écrire un programme qui affiche tous les portraits. Si les résultats produits ne sont pas les résultats
     * attendus, comprendre pourquoi et faire les modifications pour que les résultats produits soient les
     * résultats attendus.
	 */
	private static void displayPortraits() {
		Resource portrait = graphPhoto.getDataSchema().getSubject("Portrait");
		LOG.debug("Resource portrait : " + portrait);
		
		StmtIterator iterPortrait = graphPhoto.getDataSchema().getModel().listStatements(null, RDF.type, portrait);
		while(iterPortrait.hasNext()) {
			Statement stmtPortrait = iterPortrait.nextStatement();
			Resource subjectPortrait = stmtPortrait.getSubject();
			
			LOG.debug("Photo portrait : " + subjectPortrait);
		}
	}
}
