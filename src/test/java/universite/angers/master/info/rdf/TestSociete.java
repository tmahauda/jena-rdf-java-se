package universite.angers.master.info.rdf;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.RDF;
import org.apache.log4j.Logger;
import universite.angers.master.info.rdf.graph.GraphAdapter;
import universite.angers.master.info.rdf.graph.GraphFactory;

/**
 * Test sur les données de la société de productions à travers des exercices
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class TestSociete {

	private static final Logger LOG = Logger.getLogger(TestSociete.class);

	/**
	 * Propriétés schéma
	 */
	private static final String URI_SOCIETE_SCHEMA = "http://www.m1wd.org/societe";
	private static final String PATH_SOCIETE_SCHEMA = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/societe/Societe_Schema.n3";

	/**
	 * Propriétés data
	 */
	private static final String URI_SOCIETE_DATA = "http://www.m1wd.org/societe";
	private static final String PATH_SOCIETE_DATA = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/societe/Societe_Data.n3";

	/**
	 * Propriétés data schema
	 */
	private static final String URI_SOCIETE_DATA_SCHEMA = "http://www.m1wd.org/societe";
	private static final String PATH_SOCIETE_DATA_SCHEMA = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/societe/Societe_Data_Schema.n3";
	
	/**
	 * Graphe qui contient les 3 modèles
	 */
	private static GraphAdapter<OntModel, OntModel, InfModel> graphSociete;
	
	public static void main(String[] args) {
		load();
		displayProductions();
		displayClassesPeter();
	}

	/**
	 * Charge, affiche et valide le graphe
	 */
	private static void load() {
		graphSociete = GraphFactory.buildGraphOntModelFromFactory(
				PATH_SOCIETE_SCHEMA, Lang.N3, URI_SOCIETE_SCHEMA, 
				PATH_SOCIETE_DATA, Lang.N3, URI_SOCIETE_DATA, 
				PATH_SOCIETE_DATA_SCHEMA, Lang.N3, URI_SOCIETE_DATA_SCHEMA);
		
		graphSociete.read();
		graphSociete.print();
		graphSociete.valid();
	}
	
	/**
	 * Écrire un programme qui affiche toutes les productions (l’URI de la production) et pour chaque
     * production, affiche les URI des personnes qui interviennent dans la fabrication de la production. Pour
	 * chacune de ces personnes, il sera affiché « Réalisateur » si la personne a la fonction de réalisateur
	 * (même si elle n’est pas le réalisateur de cette production).
	 */
	private static void displayProductions() {
		Resource production = graphSociete.getDataSchema().getSubject("Production");
		LOG.debug("Resource production : " + production);
		
		Resource personne = graphSociete.getDataSchema().getSubject("Personne");
		LOG.debug("Resource personne : " + personne);
		
		Resource realisateur = graphSociete.getDataSchema().getSubject("Realisateur");
		LOG.debug("Resource realisateur : " + realisateur);

		//affiche toutes les productions (l’URI de la production)
		StmtIterator iterProd = graphSociete.getDataSchema().getModel().listStatements(null, RDF.type, production);
		
		while(iterProd.hasNext()) {
			Statement stmtProd = iterProd.nextStatement();
			Resource subjectProd = stmtProd.getSubject();
			
			LOG.debug("Prod subject : " + subjectProd);

			//pour chaque production, affiche les URI des personnes qui 
			//interviennent dans la fabrication de la production
			StmtIterator iterProp = subjectProd.listProperties();
			
			while(iterProp.hasNext()) {
				Statement stmtProp = iterProp.nextStatement();
				RDFNode objectProp = stmtProp.getObject();
				
				if(objectProp.isResource()) {
					
					boolean isPersonne = graphSociete.getDataSchema().resourceContainsType(objectProp.asResource(), personne);
					
					if(isPersonne) {
						String intervenant = objectProp + " intervient dans la production";
						
						boolean isRealisateur = graphSociete.getDataSchema().resourceContainsType(objectProp.asResource(), realisateur);

						if(isRealisateur) {
							intervenant += " (Réalisateur)";
						}
						
						LOG.debug(intervenant);
					}
				}
			}
		}
	}
	
	/**
	 * Écrire un programme qui affiche toutes les classes dont Peter est instance. Comprendre comment ce
	 * résultat a été obtenu, s'il est conforme à ce qui était attendu, et, s’il n’est pas conforme à ce qui était
     * attendu, se demander pourquoi (ou corriger ce qui doit l’être pour que ce soit le cas).
	 */
	private static void displayClassesPeter() {
		Resource petter = graphSociete.getDataSchema().getSubject("Petter");
		LOG.debug("Resource petter : " + petter);
		
		StmtIterator iterPetter = graphSociete.getDataSchema().getModel().listStatements(petter, RDF.type, (RDFNode)null);
		
		while(iterPetter.hasNext()) {
			Statement stmtPetter = iterPetter.nextStatement();
			
			LOG.debug("Petter est de type :" + stmtPetter.getObject());
		}
	}
}
