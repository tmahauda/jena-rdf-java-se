package universite.angers.master.info.rdf.graph.model;

import java.util.Iterator;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.reasoner.ValidityReport;
import org.apache.jena.reasoner.ValidityReport.Report;
import org.apache.log4j.Logger;
import universite.angers.master.info.rdf.read.ReaderModel;
import universite.angers.master.info.rdf.write.WriterModel;

/**
 * Stratégie qui permet de retourner un modèle abstrait avec inférence
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 *
 * @param <T> le modèle inférente maniuplé
 */
public class ModelInferenceAdapter<T extends InfModel> extends ModelAdapter<T> {

	private static final Logger LOG = Logger.getLogger(ModelInferenceAdapter.class);

	public ModelInferenceAdapter(String uri, ReaderModel<T> reader, WriterModel<T> writer) {
		super(uri, reader, writer);
	}

	@Override
	public boolean valid() {
		ValidityReport validity = this.model.validate();
		if (validity.isValid()) {
			LOG.debug("Inference model is valid");
			return true;
		} else {
			LOG.debug("Inference model is not valid");
			for (Iterator<Report> i = validity.getReports(); i.hasNext();) {
				Report report = i.next();
				LOG.debug("Report : " + report);
			}
			return false;
		}
	}

	@Override
	public String toString() {
		return "ModelInferenceAdapter [model=" + model + ", uri=" + uri + ", reader=" + reader + ", writer=" + writer
				+ "]";
	}
}
