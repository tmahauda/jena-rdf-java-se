package universite.angers.master.info.rdf.read.modeldefault;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import universite.angers.master.info.rdf.read.ReaderModel;

/**
 * Classe qui retourne un lecteur pour lire un modèle par défaut
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 *
 * @param <T> le modèle manipulé
 */
public abstract class ReaderModelDefault<T extends Model> extends ReaderModel<T> {

	public ReaderModelDefault(String path, Lang lang) {
		super(path, lang);
	}

	@Override
	public String toString() {
		return "ReaderModelDefault [path=" + path + ", lang=" + lang + "]";
	}
}