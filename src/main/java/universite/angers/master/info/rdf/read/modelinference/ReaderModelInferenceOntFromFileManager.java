package universite.angers.master.info.rdf.read.modelinference;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.util.FileManager;

/**
 * Classe qui retourne un lecteur pour lire un ont modèle inférent à partir du FileManager de Jena
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class ReaderModelInferenceOntFromFileManager extends ReaderModelInference<OntModel> {

	public ReaderModelInferenceOntFromFileManager(String path, Lang lang) {
		super(path, lang);
	}

	@Override
	public OntModel read() {
		OntModel model = ModelFactory.createOntologyModel(OntModelSpec.RDFS_MEM_TRANS_INF);

		FileManager.get().readModel(model, "file://"+this.path, this.lang.getName());
		
		return model;
	}

	@Override
	public String toString() {
		return "ReaderModelInferenceOntFromFileManager [path=" + path + ", lang=" + lang + "]";
	}
}
