package universite.angers.master.info.rdf.graph.model;

import org.apache.jena.rdf.model.Model;
import org.apache.log4j.Logger;
import universite.angers.master.info.rdf.read.ReaderModel;
import universite.angers.master.info.rdf.write.WriterModel;

/**
 * Stratégie qui permet de retourner un modèle par défaut sans inférence
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 *
 * @param <T> le modèle maniuplé
 */
public class ModelDefaultAdapter<T extends Model> extends ModelAdapter<T> {

	private static final Logger LOG = Logger.getLogger(ModelDefaultAdapter.class);

	public ModelDefaultAdapter(String uri, ReaderModel<T> reader, WriterModel<T> writer) {
		super(uri, reader, writer);
	}

	@Override
	public boolean valid() {
		// Impossible de vérifier si un modèle par défaut est valide ou non
		// Retourne vrai par défaut
		LOG.debug("Default model is valid");
		return true;
	}

	@Override
	public String toString() {
		return "ModelDefaultAdapter [model=" + model + ", uri=" + uri + ", reader=" + reader + ", writer=" + writer
				+ "]";
	}
}
