package universite.angers.master.info.rdf.graph;

/**
 * Interface qui permet de lire un modèle
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Readable {

	/**
	 * Lire un modèle
	 * @return
	 */
	public boolean read();
}
