package universite.angers.master.info.rdf.graph;

/**
 * Interface qui permet de vérifier la validité d'un modèle
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Validable {

	/**
	 * Vérifier si un modèle est valide tant sur la syntaxe que sur les contraintes imposées
	 * @return vrai si le modele est valide
	 */
	public boolean valid();
}
