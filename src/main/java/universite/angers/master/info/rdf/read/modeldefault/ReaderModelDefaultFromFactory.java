package universite.angers.master.info.rdf.read.modeldefault;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;

/**
 * Classe qui retourne un lecteur pour lire un modèle par défaut dans un fichier à partir de la Factory de Jena
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class ReaderModelDefaultFromFactory extends ReaderModelDefault<Model> {

	public ReaderModelDefaultFromFactory(String path, Lang lang) {
		super(path, lang);
	}

	@Override
	public Model read() {
		Model model = ModelFactory.createDefaultModel();
		
		model.read("file://"+this.path, this.lang.getName());
		
		return model;
	}

	@Override
	public String toString() {
		return "ReaderModelDefaultFromFactory [path=" + path + ", lang=" + lang + "]";
	}
}
