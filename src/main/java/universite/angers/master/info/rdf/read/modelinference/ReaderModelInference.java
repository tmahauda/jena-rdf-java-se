package universite.angers.master.info.rdf.read.modelinference;

import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.riot.Lang;
import universite.angers.master.info.rdf.read.ReaderModel;

/**
 * Classe qui retourne un lecteur pour lire un modèle avec inférence
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class ReaderModelInference<T extends InfModel> extends ReaderModel<T> {

	public ReaderModelInference(String path, Lang lang) {
		super(path, lang);
	}

	@Override
	public String toString() {
		return "ReaderModelInference [path=" + path + ", lang=" + lang + "]";
	}
}
